# Java Review
* Jun 19, 2017: added Java 8 in action and roll back to commit: ae8122f433f90a8bfed7244cdf6f99271f1d1125, so no JDK 1.9
* Oct 25, 2016: Added Safari Learning Path, learning xuetangx: 软件设计模式.
* Added Holub on Patterns: Game of life and HolubSQL. HolubSQL didn't compile, [LightSQL](https://github.com/xianrendzw/LightSQL) does.

## Safari Learning Path
### Advanced Java

### Java 8 in action

## Enterprise Web Programming with Java

## Learn SPRING and SPRING BOOT

## [Design Patterns in the Real World](http://shop.oreilly.com/product/0636920043942.do)
### Game of Life

### HolubSQL/LightSQL